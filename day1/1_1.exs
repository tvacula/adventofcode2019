defmodule FuelCalculator do
  def calculateFuel(mass, fuelMass) when mass > 0 do
    mass = Integer.floor_div(mass, 3) - 2

    cond do
      mass > 0 ->
        calculateFuel(mass, fuelMass + mass)
      mass <= 0 ->
        calculateFuel(mass, fuelMass)
    end
  end

  def calculateFuel(mass, fuelMass) when mass <= 0 do
    fuelMass
  end
end

defmodule ShipFuelGetter do
  def calculateShipFuelFromFile(filename) do
    {:ok, body} = File.read(filename)

    getFuelAmount = &(FuelCalculator.calculateFuel(String.to_integer(&1), 0))

    IO.puts String.split(body, "\n") |> Enum.map(getFuelAmount) |> Enum.sum
  end
end

ShipFuelGetter.calculateShipFuelFromFile("source1_1.txt")
