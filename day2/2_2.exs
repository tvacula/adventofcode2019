defmodule OpCodeResolver do
  def resolveOpCode(list, curPos) do
    cond do
      Enum.at(list, curPos) === 1 ->
        IntCodeUpdater.modifyByAddition(list, getInstruction(list, curPos))
          |> resolveOpCode(curPos + 4)
      Enum.at(list, curPos) === 2 ->
        IntCodeUpdater.modifyByPower(list, getInstruction(list, curPos))
          |> resolveOpCode(curPos + 4)
      Enum.at(list, curPos) === 99 ->
        list
      true ->
        raise "Could not resolve OpCode"
    end
  end

  defp getInstruction(list, curPos) do
    %{noun: Enum.at(list, curPos + 1), verb: Enum.at(list, curPos + 2), target: Enum.at(list, curPos + 3)}
  end
end

defmodule IntCodeUpdater do
  def modifyByAddition(list, instruction) do
    amount = Enum.at(list, instruction.noun) + Enum.at(list, instruction.verb)
    List.replace_at(list, instruction.target, amount)
  end

  def modifyByPower(list, instruction) do
    amount = Enum.at(list, instruction.noun) * Enum.at(list, instruction.verb)
    List.replace_at(list, instruction.target, amount)
  end
end

defmodule StartingInstructionModifier do
  def useCustomInstruction(list, noun, verb) do
    List.replace_at(list, 1, noun) |> List.replace_at(2, verb)
  end
end

defmodule ResultResolver do
  def findCombinationForResult(list, result, combination) do
    computedResult = StartingInstructionModifier.useCustomInstruction(list, Map.fetch!(combination, :noun), Map.fetch!(combination, :verb))
      |> OpCodeResolver.resolveOpCode(0)
      |> Enum.at(0)

    cond do
      computedResult !== result ->
        findCombinationForResult(list, result, updateCombination(combination))
      computedResult === result ->
        combination
    end
  end

  defp updateCombination(combination) do
    noun = Map.fetch!(combination, :noun)
    verb = Map.fetch!(combination, :verb)

    cond do
      noun === 99 ->
        %{combination | noun: 0, verb: verb + 1}
      noun < 99 ->
        %{combination | noun: noun + 1}
      verb === 100 ->
        raise "Combination cannot be updated further, result unreachable"
    end

  end
end

defmodule DataLoader do
  def loadFromFile(filename) do
    {:ok, body} = File.read(filename)

    String.split(body, ",") |> Enum.map(&String.to_integer(&1))
  end
end

fileInput = DataLoader.loadFromFile("source.txt")
startingCombination = %{noun: 0, verb: 0}
IO.puts inspect ResultResolver.findCombinationForResult(fileInput, 19690720, startingCombination)
