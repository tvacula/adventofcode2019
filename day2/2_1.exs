defmodule OpCodeResolver do
  def resolveOpCode(list, curPos) do
    cond do
      Enum.at(list, curPos) === 1 ->
        IntCodeUpdater.modifyByAddition(list, getInstruction(list, curPos))
          |> resolveOpCode(curPos + 4)
      Enum.at(list, curPos) === 2 ->
        IntCodeUpdater.modifyByPower(list, getInstruction(list, curPos))
          |> resolveOpCode(curPos + 4)
      Enum.at(list, curPos) === 99 ->
        list
      true ->
        raise "Could not resolve OpCode"
    end
  end

  defp getInstruction(list, curPos) do
    %{noun: Enum.at(list, curPos + 1), verb: Enum.at(list, curPos + 2), target: Enum.at(list, curPos + 3)}
  end
end

defmodule IntCodeUpdater do
  def modifyByAddition(list, instruction) do
    amount = Enum.at(list, instruction.noun) + Enum.at(list, instruction.verb)
    List.replace_at(list, instruction.target, amount)
  end

  def modifyByPower(list, instruction) do
    amount = Enum.at(list, instruction.noun) * Enum.at(list, instruction.verb)
    List.replace_at(list, instruction.target, amount)
  end
end

defmodule StartingInstructionModifier do
  def useCustomInstruction(list, noun, verb) do
    List.replace_at(list, 1, noun) |> List.replace_at(2, verb)
  end
end

defmodule DataLoader do
  def loadFromFile(filename) do
    {:ok, body} = File.read(filename)

    String.split(body, ",") |> Enum.map(&String.to_integer(&1))
  end
end

customInput = StartingInstructionModifier.useCustomInstruction(DataLoader.loadFromFile("source.txt"), 12, 2)
computedList = OpCodeResolver.resolveOpCode(customInput, 0);
IO.puts Enum.at(computedList, 0)
