defmodule Passwordbreaker do
  @data_range 136818..685979

  def get_data_range() do
    @data_range
  end

  def process_data_range() do
    for n <- get_data_range(), do: process_number(n)
  end

  def process_number(integer) do
    digits = Integer.digits(integer)
    try do
      check_incremental_or_equal_rule(digits)
      check_two_same_digits_rule(digits, %{})
      integer
    rescue
      RuntimeError ->
        nil
    end
  end

  def check_incremental_or_equal_rule([head | tail]) when tail !== [] do
    cond do
      head <= hd(tail) ->
        check_incremental_or_equal_rule(tail)
      true ->
        raise "Incremental rule not upheld"
    end
  end

  def check_incremental_or_equal_rule([_head | tail]) when tail === [] do
    true
  end

  def check_incremental_or_equal_rule([]) do
    true
  end 

  def check_two_same_digits_rule([head | tail], sameDigits) when tail !== []  do
    cond do
      head === hd(tail) ->
        check_two_same_digits_rule(tail, update_same_digits(sameDigits, hd(tail)))
      true ->
        check_two_same_digits_rule(tail, sameDigits)
    end
  end

  def check_two_same_digits_rule([_head | tail], sameDigits) when tail === [] do
    check_two_same_digits_rule([], sameDigits)
  end

  def check_two_same_digits_rule([], sameDigits) do
    isOnlyTwoTimes? = Map.values(sameDigits) |> Enum.any?(&(&1 === 2))
    if isOnlyTwoTimes? === false do
      raise "Error"
    end
  end

  defp update_same_digits(sameDigits, number) do
    case Map.has_key?(sameDigits, number) do
      true ->
        Map.replace!(sameDigits, number, Map.get(sameDigits, number) + 1)
      false ->
        Map.put_new(sameDigits, number, 2)
    end
  end
end

IO.puts inspect Passwordbreaker.process_data_range() |> Enum.filter(&(&1 !== nil)) |> Enum.count