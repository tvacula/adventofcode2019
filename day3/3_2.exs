defmodule InstructionSet do
  defstruct [:first, :second]

  def from_source(tuple) do
    %InstructionSet{
      first: elem(tuple, 0) |> format_instruction,
      second: elem(tuple, 1) |> format_instruction
    }
  end

  def find_intersections(lineSet) do
    Enum.filter(lineSet.first, fn elem -> Enum.find(lineSet.second, nil, &(&1 === elem && elem !== {0, 0})) end) |> get_closest_distance(lineSet) |> Enum.min
  end

  def get_closest_distance(list, lineSet) do
    Enum.map(list, fn element -> Enum.find_index(lineSet.first, &(&1 === element)) + Enum.find_index(lineSet.second, &(&1 === element)) end)
  end

  defp format_instruction(instruction) do
    instruction
      |> String.split(",")
      |> Enum.map(fn elem -> String.split_at(elem, 1) end)
  end
end

defmodule LineSet do
  defstruct [:first, :second]

  def from_instruction_set(instructionSet) do
    %LineSet{
      first: Map.get(instructionSet, :first) |> CoordinatesTransformer.from_instructions([{0, 0}], {0, 0}),
      second: Map.get(instructionSet, :second) |> CoordinatesTransformer.from_instructions([{0, 0}], {0, 0})
    }
  end
end

defmodule DirectionResolver do
  def resolve(coordinatesList, direction, value, lastCoordinates) do
    case direction do
      "R" ->
        process_by_adding_x(coordinatesList, value, lastCoordinates)
      "L" ->
        process_by_decreasing_x(coordinatesList, value, lastCoordinates)
      "U" ->
        process_by_adding_y(coordinatesList, value, lastCoordinates)
      "D" ->
        process_by_decreasing_y(coordinatesList, value, lastCoordinates)
      _ ->
        raise "Unmatchable direction provided"
    end
  end

  def process_by_adding_x(coordinatesList, times, lastCoordinates) when times > 0 do
    newCoordinates = {elem(lastCoordinates, 0) + 1, elem(lastCoordinates, 1)}
    coordinatesList ++ [newCoordinates] |> process_by_adding_x(times - 1, newCoordinates)
  end

  def process_by_adding_x(coordinatesList, times, lastCoordinates) when times === 0 do
    {coordinatesList, lastCoordinates}
  end

  def process_by_adding_y(coordinatesList, times, lastCoordinates) when times > 0 do
    newCoordinates = {elem(lastCoordinates, 0), elem(lastCoordinates, 1) + 1}
    coordinatesList ++ [newCoordinates] |> process_by_adding_y(times - 1, newCoordinates)
  end

  def process_by_adding_y(coordinatesList, times, lastCoordinates) when times === 0 do
    {coordinatesList, lastCoordinates}
  end

  def process_by_decreasing_x(coordinatesList, times, lastCoordinates) when times > 0 do
    newCoordinates = {elem(lastCoordinates, 0) - 1, elem(lastCoordinates, 1)}
    coordinatesList ++ [newCoordinates] |> process_by_decreasing_x(times - 1, newCoordinates)
  end

  def process_by_decreasing_x(coordinatesList, times, lastCoordinates) when times === 0 do
    {coordinatesList, lastCoordinates}
  end

  def process_by_decreasing_y(coordinatesList, times, lastCoordinates) when times > 0 do
    newCoordinates = {elem(lastCoordinates, 0), elem(lastCoordinates, 1) - 1}
    coordinatesList ++ [newCoordinates] |> process_by_decreasing_y(times - 1, newCoordinates)
  end

  def process_by_decreasing_y(coordinatesList, times, lastCoordinates) when times === 0 do
    {coordinatesList, lastCoordinates}
  end
end

defmodule CoordinatesTransformer do
  def from_instructions([head | tail], coordinatesList, lastCoordinates) do
    updatedCoordinates = DirectionResolver.resolve(coordinatesList, elem(head, 0), elem(head, 1) |> String.to_integer, lastCoordinates)
    from_instructions(tail, elem(updatedCoordinates, 0), elem(updatedCoordinates, 1))
  end

  def from_instructions([], coordinatesList, _lastCoordinates) do
    coordinatesList
  end
end

defmodule DataLoader do
  def load_from_file(filename) do
    {:ok, body} = File.read(filename)

    String.split(body, "\n")
     |> List.to_tuple
     |> InstructionSet.from_source
  end
end

IO.puts inspect DataLoader.load_from_file("source.txt") |> LineSet.from_instruction_set |> InstructionSet.find_intersections
